const db = require('../models');
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

async function init() {
    await sleep(5000);
    console.log("ok1")
    const rescue = await db.Rescue.create({
        validated: true,
        title: "Sauvetage du GRACE DE DIEU",
        subtitle: "1 personne sauvée",
        duration: null,
        description: "Dunkerque. Un bateau de pêche de Bray-Dunes, le sloop GRACE DE DIEU, N°1372, a abordé ce matin, vers dix heures, le feu flottant SNOW, qui est mouillé dans le N.N.O. de Dunkerque. Le bateau coula presque aussitôt. Des quatre matelots qui le montaient, un seul, Théophile Vanhille, âgé de 17 ans, fut sauvé par l’équipage et le patron Vanhille du dundee de pêche JESUS MARIE N°1408 . Les trois autres marins , Louis Martel, 37 ans, patron ; Julien Marteau, 18 ans, et le mousse Henri Gryson se sont noyés. L’armateur du bateau est le père du survivant, un marin qui avait eu le bras enlevé d’un coup de feu dernièrement.",
        dateRescue: new Date(1910, 05, 27),
        sources: "Grand Echo du Nord de la France 27 mai 1910"
    });
    const boatRescued = await db.Boat.create({
        validated: true,
        name: "GRACE DE DIEU",
        type: "sloop",
        manufacturer: null,
        manufacturingDate: null,
        length: null,
        width: null,
        weight: null,
        endDuty: "27 mai 1910",
        description: null,
        isRescued: true,
        isRescuer: false
    });
    const boatRescuer = await db.Boat.create({
        validated: true,
        name: "JESUS MARIE",
        type: "dundee de pêche",
        manufacturer: null,
        manufacturingDate: null,
        length: null,
        width: null,
        weight: null,
        endDuty: null,
        description: null,
        isRescued: false,
        isRescuer: true
    });
    const rescued = await db.Rescued.create({
        validated: true,
        lastname: "Vanhille",
        firstname: "Théophile",
        birthDay: new Date(1893,00,00),
        description: null,
    });
    rescue.addBoat(boatRescued);
    rescue.addBoat(boatRescuer);
    rescue.addRescued(rescued);


}

module.exports = {init}