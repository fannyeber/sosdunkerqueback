const { Sequelize, DataTypes } = require('sequelize');

module.exports = sequelize => {

	class Boat extends Sequelize.Model {
		static associate(db) {
         Boat.belongsToMany(db.Rescue, { through: 'RescueBoat' });
		}
	}

	Boat.init({
        validated: DataTypes.BOOLEAN,
        name: DataTypes.STRING,
        type: DataTypes.STRING,
        manufacturer: DataTypes.STRING,
        manufacturingDate: DataTypes.DATE,
        length: DataTypes.DOUBLE,
        width: DataTypes.DOUBLE,
        weight: DataTypes.DOUBLE,
        endDuty: DataTypes.STRING,
        description: DataTypes.TEXT,
        isRescued: DataTypes.BOOLEAN,
        isRescuer: DataTypes.BOOLEAN,
    }, {
		sequelize,
		modelName: 'Boat'
	});
  

	return Boat;

};