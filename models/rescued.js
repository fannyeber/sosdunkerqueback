const { Sequelize, DataTypes } = require('sequelize');

module.exports = sequelize => {

    class Rescued extends Sequelize.Model {
        static associate(db) {
            Rescued.belongsToMany(db.Rescue, { through: 'RescueRescued' });

        }
    }

    Rescued.init({
        validated: DataTypes.BOOLEAN,
        lastname: DataTypes.STRING,
        firstname: DataTypes.STRING,
        birthDay: DataTypes.DATE,
        description: DataTypes.TEXT,
    }, {
        sequelize,
        modelName: 'Rescued'
    });

    return Rescued;

};
