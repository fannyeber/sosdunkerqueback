const { Sequelize, DataTypes } = require('sequelize');

module.exports = sequelize => {

	class Rescue extends Sequelize.Model {
		static associate(db) {
			Rescue.belongsToMany(db.Rescued, { through: 'RescueRescued' });
			Rescue.belongsToMany(db.Rescuer, { through: 'RescueRescuer' });
			Rescue.belongsToMany(db.Boat, { through: 'RescueBoat' });
		}
	}

	Rescue.init({
		validated: DataTypes.BOOLEAN,
		title: DataTypes.STRING,
		subtitle: DataTypes.STRING,
		duration: DataTypes.TIME,
        description: DataTypes.TEXT,
        dateRescue: DataTypes.DATE,
		sources: DataTypes.TEXT
    }, {
		sequelize,
		modelName: 'Rescue'
	});

	return Rescue;

};