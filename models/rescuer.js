const { Sequelize, DataTypes } = require('sequelize');

module.exports = sequelize => {

    class Rescuer extends Sequelize.Model {
        static associate(db) {
            Rescuer.belongsToMany(db.Rescue, { through: 'RescueRescuer' });

        }
    }

    Rescuer.init({
        validated: DataTypes.BOOLEAN,
        lastname: DataTypes.STRING,
        firstname: DataTypes.STRING,
        birthDay: DataTypes.DATE,
        description: DataTypes.TEXT,

        grade: DataTypes.STRING,
        maritalStatus: DataTypes.STRING,
        genealogicalData: DataTypes.DATE,
        career: DataTypes.TEXT,
        professionalLife: DataTypes.TEXT,
        decoration: DataTypes.TEXT,
        detail: DataTypes.TEXT
    }, {
        sequelize,
        modelName: 'Rescuer'
    });

    return Rescuer;

};
