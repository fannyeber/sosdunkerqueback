const rescued_ctrl = require('../controllers/rescued');

module.exports = [

	{
		url: '/rescued',
		method: 'get',
		func: rescued_ctrl.get_all
	},
	{
		url: '/rescued',
		method: 'post',
		func: rescued_ctrl.create
	},
	{
		url: '/rescued/:rescued_id',
		method: 'get',
		func: rescued_ctrl.get_by_id
	},
	{
		url: '/rescued/:rescued_id',
		method: 'delete',
		func: rescued_ctrl.delete_by_id
	},
	{
        url: '/rescued/validate/:rescued_id',
        method: 'patch',
        func: rescued_ctrl.validate_by_id
    }

];
