const rescue_ctrl = require('../controllers/rescue');
const rescuer_ctrl = require('../controllers/rescuer');
const rescued_ctrl = require('../controllers/rescued');
const boat_ctrl = require('../controllers/boat');

module.exports = [

	{
		url: '/rescue',
		method: 'get',
		func: rescue_ctrl.get_all
	},
	{
		url: '/rescue/:rescue_id/rerscueds',
		method: 'get',
		func: [rescue_ctrl.load_by_id, rescue_ctrl.get_all_rescueds]
	},
	{
		url: '/rescue/:rescue_id/rescuers',
		method: 'get',
		func: [rescue_ctrl.load_by_id, rescue_ctrl.get_all_rescuers]
	},
	{
		url: '/rescue/:rescue_id/boats',
		method: 'get',
		func: [rescue_ctrl.load_by_id, rescue_ctrl.get_all_boats]
	},
	{
		url: '/rescue/:rescue_id/rescued/:rescued_id',
		method: 'post',
		func: [rescue_ctrl.load_by_id, rescued_ctrl.load_by_id, rescue_ctrl.add_rescued]
	},
	{
		url: '/rescue/:rescue_id/rescuer/:rescuer_id',
		method: 'post',
		func: [rescue_ctrl.load_by_id, rescuer_ctrl.load_by_id, rescue_ctrl.add_rescuer]
	},
	{
		url: '/rescue/:rescue_id/boat/:boat_id',
		method: 'post',
		func: [rescue_ctrl.load_by_id, boat_ctrl.load_by_id, rescue_ctrl.add_boat]
	},
	{
		url: '/rescue',
		method: 'post',
		func: rescue_ctrl.create
	},
	{
		url: '/rescue/:rescue_id',
		method: 'get',
		func: rescue_ctrl.get_by_id
	},
	{
		url: '/rescue/:rescue_id',
		method: 'delete',
		func: rescue_ctrl.delete_by_id
	},
	{
        url: '/rescue/validate/:rescue_id',
        method: 'patch',
        func: rescue_ctrl.validate_by_id
    }

];
