const rescuer_ctrl = require('../controllers/rescuer');

module.exports = [

	{
		url: '/rescuer',
		method: 'get',
		func: rescuer_ctrl.get_all
	},
	{
		url: '/rescuer',
		method: 'post',
		func: rescuer_ctrl.create
	},
	{
		url: '/rescuer/:rescuer_id',
		method: 'get',
		func: rescuer_ctrl.get_by_id
	},
	{
		url: '/rescuer/:rescuer_id',
		method: 'delete',
		func: rescuer_ctrl.delete_by_id
	},
	{
        url: '/rescuer/validate/:rescuer_id',
        method: 'patch',
        func: rescuer_ctrl.validate_by_id
    }

];
