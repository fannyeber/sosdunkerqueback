const boat_ctrl = require('../controllers/boat');

module.exports = [

	{
		url: '/boat',
		method: 'get',
		func: boat_ctrl.get_all
	},
    {
        url: '/boat/rescued',
        method: 'get',
        func: boat_ctrl.get_all_rescued
    },
    {
        url: '/boat/rescuer',
        method: 'get',
        func: boat_ctrl.get_all_rescuer
    },
    {
		url: '/boat',
		method: 'post',
		func: boat_ctrl.create
	},
    {
		url: '/boat/:boat_id',
		method: 'get',
		func: boat_ctrl.get_by_id
	},
    {
		url: '/boat/:boat_id',
		method: 'delete',
		func: boat_ctrl.delete_by_id
	},
    {
		url: '/boat/validate/:boat_id',
		method: 'patch',
		func: boat_ctrl.validate_by_id
	}


];
