const db = require('../models');

module.exports = {

	get_all: (req, res, next) => {
        const where = {}
        where.validated = true;
        console.log("Beffor");
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
		db.Boat.findAll({where})
			.then(boats => res.json(boats))
			.catch(next);
	},

	get_by_id: (req, res, next) => {
		return db.Boat.findByPk(req.params.boat_id)
			.then(boat => {
				if (!boat) {
					throw { status: 404, message: 'Requested Boat not found' };
				}
				return res.json(boat);
			})
			.catch(next);
	},

    load_by_id: (req, res, next) => {
		return db.Boat.findByPk(req.params.boat_id)
			.then(boat => {
				if (!boat) {
					throw { status: 404, message: 'Requested Boat not found' };
				}
				req.boat = boat;
				return next();
			})
			.catch(next);
	},

    get_all_rescuer: (req, res, next) => {
        const where = {}
        where.validated = true;
        where.isRescuer = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
        return db.Boat.findAll({where})
            .then(boats => res.json(boats))
            .catch(next);
    },

    get_all_rescued: (req, res, next) => {
        const where = {}
        where.validated = true;
        where.isRescued = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
        return db.Boat.findAll({where})
            .then(boats => res.json(boats))
            .catch(next);
    },

    create: (req, res, next) => {
		return db.Boat.create(req.body)
			.then(boat => res.json(boat))
			.catch(next);
		
	},

    delete_by_id: (req, res, next) => {
		return db.Boat.findByPk(req.params.boat_id)
			.then(boat => {
				if (!boat) {
					throw { status: 404, message: 'Requested Boat not found' };
				}
				return boat.destroy();
			})
			.then(() => res.status(200).end())
			.catch(next);
	},

    validate_by_id: (req, res, next) => {
        return db.Boat.findByPk(req.params.boat_id)
            .then(boat => {
                if (!boat) {
                    throw { status: 404, message: 'Requested Boat not found' };
                }
                return boat.update({ validated: true });
            })
            .then(() => res.status(200).end())
            .catch(next);
    }
	

};
