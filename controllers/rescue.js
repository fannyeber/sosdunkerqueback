const db = require('../models');

module.exports = {

	get_all: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
		return db.Rescue.findAll({where})
			.then(rescues => res.json(rescues))
			.catch(next);
	},

	get_by_id: (req, res, next) => {
		return db.Rescue.findByPk(req.params.rescue_id)
			.then(rescue => {
				if (!rescue) {
					throw { status: 404, message: 'Requested Rescue not found' };
				}
				return res.json(rescue);
			})
			.catch(next);
	},

    load_by_id: (req, res, next) => {
		return db.Rescue.findByPk(req.params.rescue_id)
			.then(rescue => {
				if (!rescue) {
					throw { status: 404, message: 'Requested Rescue not found' };
				}
				req.rescue = rescue;
                return next();
			})
			.catch(next);
	},

    get_all_rescuers: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated  === "true";
        }
        return req.rescue.getRescuers({where})
            .then(rescuers => res.json(rescuers))
            .catch(next);
    },

    get_all_rescueds: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
        return req.rescue.getRescueds({where})
            .then(rescueds => res.json(rescueds))
            .catch(next);
    },

    get_all_boats: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
        return req.rescue.getBoats({where})
            .then(boats => res.json(boats))
            .catch(next);
    },

    add_rescuer : (req, res, next) => {
        return req.rescue.addRescuer(req.rescuer)
        .then(rescuer => res.json(rescuer))
        .catch(next);
    },

    add_rescued : (req, res, next) => {
        return req.rescue.addRescued(req.rescued)
        .then(rescued => res.json(rescued))
        .catch(next);
    },

    add_boat : (req, res, next) => {
        return req.rescue.addBoat(req.boat)
        .then(boat => res.json(boat))
        .catch(next);
    },

    create: (req, res, next) => {
		return db.Rescue.create(req.body)
			.then(rescue => res.json(rescue))
			.catch(next);
		
	},

    delete_by_id: (req, res, next) => {
		return db.Rescue.findByPk(req.params.rescue_id)
			.then(rescue => {
				if (!rescue) {
					throw { status: 404, message: 'Requested Rescue not found' };
				}
				return rescue.destroy();
			})
			.then(() => res.status(200).end())
			.catch(next);
	},

    validate_by_id: (req, res, next) => {
        return db.Rescue.findByPk(req.params.rescue_id)
            .then(rescue => {
                if (!rescue) {
                    throw { status: 404, message: 'Requested Rescue not found' };
                }
                return rescue.update({ validated: true });
            })
            .then(() => res.status(200).end())
            .catch(next);
    }
	

};
