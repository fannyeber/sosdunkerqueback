const db = require('../models');

module.exports = {

	get_all: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === 'true';
        }
        return db.Rescuer.findAll({where})
            .then(rescuer => res.json(rescuer))
            .catch(next);
    },

	validate_by_id: (req, res, next) => {
        return db.Rescuer.findByPk(req.params.rescuer_id)
            .then(rescuer => {
                if (!rescuer) {
                    throw { status: 404, message: 'Requested Rescuer not found' };
                }
                return rescuer.update({ validated: true });
            })
            .then(() => res.status(200).end())
            .catch(next);
    },

	get_by_id: (req, res, next) => {
		return db.Rescuer.findByPk(req.params.rescuer_id)
			.then(rescuer => {
				if (!rescuer) {
					throw { status: 404, message: 'Requested Rescuer not found' };
				}
				return res.json(rescuer);
			})
			.catch(next);
	},

	load_by_id: (req, res, next) => {
		return db.Rescuer.findByPk(req.params.rescuer_id)
			.then(rescuer => {
				if (!rescuer) {
					throw { status: 404, message: 'Requested Rescuer not found' };
				}
				req.rescuer = rescuer;
				return next();
			})
			.catch(next);
	},

	create: (req, res, next) => {
		return db.Rescuer.create(req.body)
			.then(rescuer => res.json(rescuer))
			.catch(next);
	},

	delete_by_id: (req, res, next) => {
		return db.Rescuer.findByPk(req.params.rescuer_id)
			.then(rescuer => {
				if (!rescuer) {
					throw { status: 404, message: 'Requested Rescuer not found' };
				}
				return rescuer.destroy();
			})
			.then(() => res.status(200).end())
			.catch(next);
	}
};
