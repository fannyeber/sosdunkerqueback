const db = require('../models');

module.exports = {

	get_all: (req, res, next) => {
        const where = {}
        where.validated = true;
        if (req.query.validated !== undefined) {
            where.validated = req.query.validated === "true";
        }
        return db.Rescued.findAll({where})
            .then(rescued => res.json(rescued))
            .catch(next);
    },

	validate_by_id: (req, res, next) => {
        return db.Rescued.findByPk(req.params.rescued_id)
            .then(rescued => {
                if (!rescued) {
                    throw { status: 404, message: 'Requested Rescued not found' };
                }
                return rescued.update({ validated: true });
            })
            .then(() => res.status(200).end())
            .catch(next);
    },

	get_by_id: (req, res, next) => {
		return db.Rescued.findByPk(req.params.rescued_id)
			.then(rescued => {
				if (!rescued) {
					throw { status: 404, message: 'Requested Rescued not found' };
				}
				return res.json(rescued);
			})
			.catch(next);
	},

	load_by_id: (req, res, next) => {
		return db.Rescued.findByPk(req.params.rescued_id)
			.then(rescued => {
				if (!rescued) {
					throw { status: 404, message: 'Requested Rescued not found' };
				}
				req.rescued = rescued;
				return next();
			})
			.catch(next);
	},

	create: (req, res, next) => {
		return db.Rescued.create(req.body)
			.then(rescued => res.json(rescued))
			.catch(next);
	},

	delete_by_id: (req, res, next) => {
		return db.Rescued.findByPk(req.params.rescued_id)
			.then(rescued => {
				if (!rescued) {
					throw { status: 404, message: 'Requested Rescued not found' };
				}
				return rescued.destroy();
			})
			.then(() => res.status(200).end())
			.catch(next);
	}
};
